#[cfg(test)]
mod test {
    use crate::*;
    use std::path::{Path, PathBuf};

    #[test]
    fn add_command_test() {
        let config_file = "/tmp/add_command_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from([
            "ea",
            "add",
            "echo",
            "echo",
            "hello",
            ">",
            "tests/add_command_test/out",
        ]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello > tests/add_command_test/out");
    }

    #[test]
    fn remove_command_test() {
        let config_file = "/tmp/remove_command_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo");

        let mut cli_remove = Cli::parse_from(["ea", "remove", "echo"]);
        cli_remove.config_file = PathBuf::from(config_file);
        cli_remove.process().unwrap();
        let cli_get = cli.retrieve_command("echo");
        assert!(cli_get.is_err());
    }

    #[test]
    fn add_command_with_positional_subs_test() {
        let config_file = "/tmp/add_command_with_positional_subs_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{}}", "{{}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{}} {{}}");
    }

    #[test]
    fn add_command_with_named_subs_test() {
        let config_file = "/tmp/add_command_with_named_subs_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}}");
    }
    #[test]
    fn add_command_with_named_subs_repeat_test() {
        let config_file = "/tmp/add_command_with_named_subs_repeat_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}", "{{a}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}} {{a}}");
    }

    #[test]
    fn add_command_with_mixed_subs_fails_test() {
        let config_file = "/tmp/add_command_with_mixed_subs_fails_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{}}", "{{a}}"]);
        cli.config_file = PathBuf::from(config_file);
        assert!(cli.process().is_err());
        assert!(cli.retrieve_command("echo").is_err());
    }

    #[test]
    fn perform_positional_subs_on_positional_config_test() {
        let config_file = "/tmp/perform_positional_subs_on_positional_config_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{}}", "{{}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{}} {{}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "123", "456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd).unwrap();
        assert_eq!(new_cmd, "echo 123 456");
    }

    #[test]
    fn perform_positional_subs_on_named_config_test() {
        let config_file = "/tmp/perform_positional_subs_on_named_config_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "123", "456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd).unwrap();
        assert_eq!(new_cmd, "echo 123 456");
    }

    #[test]
    fn perform_named_subs_on_named_config_test() {
        let config_file = "/tmp/perform_named_subs_on_named_config_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "b=123", "a=456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo 456 123");
    }

    #[test]
    fn perform_named_subs_on_named_config_with_repeats_test() {
        let config_file = "/tmp/perform_named_subs_on_named_config_with_repeats_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from([
            "ea", "add", "echo", "echo", "{{a}}", "{{b}}", "{{a}}", "{{b}}",
        ]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}} {{a}} {{b}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "b=123", "a=456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo 456 123 456 123");
    }

    #[test]
    fn perform_named_subs_on_positional_config_fails_test() {
        let config_file = "/tmp/perform_named_subs_on_positional_config_fails_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{}}", "{{}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{}} {{}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "b=123", "a=456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());
    }

    #[test]
    fn perform_named_subs_on_named_config_equal_signs_test() {
        let config_file = "/tmp/perform_named_subs_on_named_config_equal_signs_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}", "{{c}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}} {{c}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "a==123", "b=456", "c=789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());

        let mut cli_ex = Cli::parse_from(["ea", "echo", "a===123", "b=456", "c=789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo =123 456 789");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "a===1====2====3==", "b=456", "c=789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo =1==2==3= 456 789");
    }

    #[test]
    fn perform_positional_subs_on_named_config_equal_signs_test() {
        let config_file = "/tmp/perform_positional_subs_on_named_config_equal_signs_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}", "{{c}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}} {{c}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "==123", "b=456", "c=789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());

        let mut cli_ex = Cli::parse_from(["ea", "echo", "==123", "====456", "c====789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo =123 ==456 c==789");

        let mut cli_ex = Cli::parse_from([
            "ea",
            "echo",
            "a==1====2====3==",
            "==4======5======6==",
            "c==789",
        ]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo a=1==2==3= =4===5===6= c=789");
    }

    #[test]
    fn perform_positional_subs_on_named_config_repeat_names_fails_test() {
        let config_file =
            "/tmp/perform_positional_subs_on_named_config_repeat_names_fails_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{a}}", "{{b}}", "{{a}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{a}} {{b}} {{a}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "123", "456", "789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());

        let mut cli_ex = Cli::parse_from(["ea", "echo", "123", "456"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());

        let mut cli_ex = Cli::parse_from(["ea", "echo", "123"]);
        cli_ex.config_file = PathBuf::from(config_file);
        assert!(cli_ex.perform_subs(cmd.clone()).is_err());
    }

    #[test]
    fn perform_substitutions_on_variables_named_with_numbers_test() {
        let config_file = "/tmp/perform_substitutions_on_variables_named_with_numbers_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{1a}}", "{{2}}", "{{c3}}"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo {{1a}} {{2}} {{c3}}");

        let mut cli_ex = Cli::parse_from(["ea", "echo", "c3=123", "1a=456", "2=789"]);
        cli_ex.config_file = PathBuf::from(config_file);
        let new_cmd = cli_ex.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo 456 789 123");
    }

    #[test]
    fn non_alphanumeric_variables_fails_test() {
        let config_file = "/tmp/non_alphanumeric_variables_fails_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{1 a}}", "{{2}}", "{{c3}}"]);
        cli.config_file = PathBuf::from(config_file);
        assert!(cli.process().is_err());

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{ }}"]);
        cli.config_file = PathBuf::from(config_file);
        assert!(cli.process().is_err());

        let mut cli = Cli::parse_from(["ea", "add", "echo", "echo", "{{/}}"]);
        cli.config_file = PathBuf::from(config_file);
        assert!(cli.process().is_err());
    }

    #[test]
    fn add_command_with_secure_flag_is_encrypted_test() {
        let config_file = "/tmp/add_command_with_secure_flag_is_encrypted_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli.config_file = PathBuf::from(config_file);
        cli.process().unwrap();
        let (cmd, _) = cli.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let file_contents = std::fs::read(config_file).unwrap();
        let file_str = String::from_utf8_lossy(&file_contents);
        assert_eq!(file_str, "echo_secure::[ENCRYPTED]");
    }

    #[test]
    fn add_command_with_secure_flag_doesnt_affect_other_command_test() {
        let config_file = "/tmp/add_command_with_secure_flag_doesnt_affect_other_command_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_insecure = Cli::parse_from(["ea", "add", "echo", "echo", "hello"]);
        cli_insecure.config_file = PathBuf::from(config_file);
        cli_insecure.process().unwrap();

        let mut cli_secure =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();

        let (cmd, _) = cli_secure.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello");

        let (cmd, _) = cli_secure.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let file_contents = std::fs::read(config_file).unwrap();
        let file_str = String::from_utf8_lossy(&file_contents);
        assert_eq!(file_str, "echo::echo hello\necho_secure::[ENCRYPTED]");
    }

    #[test]
    fn list_commands_without_secure_flag_doesnt_decrypt_test() {
        let config_file = "/tmp/list_commands_without_secure_flag_doesnt_decrypt_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_insecure = Cli::parse_from(["ea", "add", "echo", "echo", "hello"]);
        cli_insecure.config_file = PathBuf::from(config_file);
        cli_insecure.process().unwrap();

        let mut cli_secure =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();

        let (cmd, _) = cli_secure.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello");

        let (cmd, _) = cli_secure.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let mut cli_list = Cli::parse_from(["ea", "list"]);
        cli_list.config_file = PathBuf::from(config_file);
        let res = cli_list.process().unwrap().unwrap();
        assert_eq!(res, "echo::echo hello\necho_secure::[ENCRYPTED]")
    }

    #[test]
    fn list_commands_with_secure_flag_does_decrypt_test() {
        let config_file = "/tmp/list_commands_with_secure_flag_does_decrypt_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_insecure = Cli::parse_from(["ea", "add", "echo", "echo", "hello"]);
        cli_insecure.config_file = PathBuf::from(config_file);
        cli_insecure.process().unwrap();

        let mut cli_secure =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();

        let (cmd, _) = cli_secure.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello");

        let (cmd, _) = cli_secure.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let mut cli_list = Cli::parse_from(["ea", "list", "-s"]);
        cli_list.config_file = PathBuf::from(config_file);
        let res = cli_list.process().unwrap().unwrap();
        assert_eq!(res, "echo::echo hello\necho_secure::echo hello secure")
    }

    #[test]
    fn show_command_with_secure_alias_decrypts_test() {
        let config_file = "/tmp/show_command_with_secure_alias_decrypts_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_secure =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();
        let (cmd, _) = cli_secure.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let mut cli_show = Cli::parse_from(["ea", "show", "echo_secure"]);
        cli_show.config_file = PathBuf::from(config_file);
        let res = cli_show.process().unwrap().unwrap();
        assert_eq!(res, "echo hello secure")
    }

    #[test]
    fn show_command_with_insecure_alias_shows_test() {
        let config_file = "/tmp/show_command_with_insecure_alias_shows_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_insecure = Cli::parse_from(["ea", "add", "echo", "echo", "hello"]);
        cli_insecure.config_file = PathBuf::from(config_file);
        cli_insecure.process().unwrap();
        let (cmd, _) = cli_insecure.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello");

        let mut cli_show = Cli::parse_from(["ea", "show", "echo"]);
        cli_show.config_file = PathBuf::from(config_file);
        let res = cli_show.process().unwrap().unwrap();
        assert_eq!(res, "echo hello")
    }

    #[test]
    fn encrypt_command_encrypts_command_test() {
        let config_file = "/tmp/encrypt_command_encrypts_command_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_insecure = Cli::parse_from(["ea", "add", "echo", "echo", "hello"]);
        cli_insecure.config_file = PathBuf::from(config_file);
        cli_insecure.process().unwrap();
        let (cmd, _) = cli_insecure.retrieve_command("echo").unwrap();
        assert_eq!(cmd, "echo hello");

        let mut cli_encrypt = Cli::parse_from(["ea", "encrypt", "echo"]);
        cli_encrypt.config_file = PathBuf::from(config_file);
        cli_encrypt.process().unwrap();

        let file_contents = std::fs::read(config_file).unwrap();
        let file_str = String::from_utf8_lossy(&file_contents);
        assert_eq!(file_str, "echo::[ENCRYPTED]");

        let mut cli_show = Cli::parse_from(["ea", "show", "echo"]);
        cli_show.config_file = PathBuf::from(config_file);
        let res = cli_show.process().unwrap().unwrap();
        assert_eq!(res, "echo hello")
    }

    #[test]
    fn decrypt_command_decrypts_command_test() {
        let config_file = "/tmp/decrypt_command_decrypts_command_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_secure =
            Cli::parse_from(["ea", "add", "-s", "echo_secure", "echo", "hello", "secure"]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();
        let (cmd, _) = cli_secure.retrieve_command("echo_secure").unwrap();
        assert_eq!(cmd, "echo hello secure");

        let mut cli_decrypt = Cli::parse_from(["ea", "decrypt", "echo_secure"]);
        cli_decrypt.config_file = PathBuf::from(config_file);
        cli_decrypt.process().unwrap();

        let file_contents = std::fs::read(config_file).unwrap();
        let file_str = String::from_utf8_lossy(&file_contents);
        assert_eq!(file_str, "echo_secure::echo hello secure");

        let mut cli_show = Cli::parse_from(["ea", "show", "echo_secure"]);
        cli_show.config_file = PathBuf::from(config_file);
        let res = cli_show.process().unwrap().unwrap();
        assert_eq!(res, "echo hello secure")
    }

    #[test]
    fn encrypted_command_with_subs_works_test() {
        let config_file = "/tmp/encrypted_command_with_subs_works_test/earc";
        if Path::try_exists(Path::new(config_file)).unwrap() {
            std::fs::remove_file(config_file).unwrap();
        }

        let mut cli_secure = Cli::parse_from([
            "ea",
            "add",
            "-s",
            "echo_secure_subs",
            "echo",
            "{{x}}",
            "secure",
        ]);
        cli_secure.config_file = PathBuf::from(config_file);
        cli_secure.process().unwrap();
        let (cmd, _) = cli_secure.retrieve_command("echo_secure_subs").unwrap();
        assert_eq!(cmd, "echo {{x}} secure");

        let mut cli_subs = Cli::parse_from(["ea", "echo_secure_subs", "x=test"]);
        cli_subs.config_file = PathBuf::from(config_file);
        let new_cmd = cli_subs.perform_subs(cmd.clone()).unwrap();
        assert_eq!(new_cmd, "echo test secure");

        let mut cli_subs = Cli::parse_from(["ea", "echo_secure_subs", "test"]);
        cli_subs.config_file = PathBuf::from(config_file);
        let new_cmd = cli_subs.perform_subs(cmd).unwrap();
        assert_eq!(new_cmd, "echo test secure");
    }
}

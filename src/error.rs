use std::fmt;
use std::fmt::Formatter;

#[derive(Debug)]
pub enum CliError<'a> {
    AliasNotFound(&'a str),
    AllNamedRequired(Vec<String>),
    CommandRequired,
    ConfigDirIsFile,
    FileRead(String),
    InvalidVariables(Vec<String>, Vec<String>),
    IoError(String),
    Keyring(String),
    MixingSubTypes(Vec<String>),
    ReservedKeyword,
    SubsMismatch(Vec<String>, &'a Vec<String>),
}

impl fmt::Display for CliError<'_> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        let err_str = match self {
            CliError::AliasNotFound(a) => format!("AliasNotFoundError: {a}"),
            CliError::AllNamedRequired(a) => format!("Error: This alias requires all substitutions to be named arguments due to repeated variables.\nStored substitutions: {a:?}"),
            CliError::CommandRequired => format!("Error: You need to input a command that the alias will invoke."),
            CliError::ConfigDirIsFile => format!("Error: The configuration directory should be located at ~/.config/ea, but a file exists there."),
            CliError::FileRead(e) => format!("Error: {e}"),
            CliError::InvalidVariables(all, valid) => format!("Error: Invalid variable names. Variable names must only include alphanumerics."),
            CliError::IoError(e) => format!("Error: {e}"),
            CliError::Keyring(e) => format!("KeyringError: {e}"),
            CliError::MixingSubTypes(s) => format!("Error: Must use only positional substitutions or only named substitutions.\nInput substitutions: {s:?}"),
            CliError::ReservedKeyword => format!("Error: Cannot make an alias that matches a subcommand for easy_alias."),
            CliError::SubsMismatch(a, b) => format!("Error: Variables do not match stored command.\nStored variables: {a:?}\nInput variables: {b:?}"),
        };
        write!(f, "{}", err_str)
    }
}

impl From<std::string::FromUtf8Error> for CliError<'static> {
    fn from(e: std::string::FromUtf8Error) -> CliError<'static> {
        CliError::FileRead(e.to_string())
    }
}

impl From<std::io::Error> for CliError<'static> {
    fn from(e: std::io::Error) -> CliError<'static> {
        CliError::IoError(e.to_string())
    }
}

impl From<rustyline::error::ReadlineError> for CliError<'static> {
    fn from(e: rustyline::error::ReadlineError) -> CliError<'static> {
        CliError::IoError(e.to_string())
    }
}

impl From<keyring::error::Error> for CliError<'static> {
    fn from(e: keyring::error::Error) -> CliError<'static> {
        CliError::Keyring(e.to_string())
    }
}

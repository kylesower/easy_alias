use crate::error::CliError;
use clap::{Args, Parser, Subcommand};
use keyring::{self, mock, Entry};
use regex::Regex;
use std::path::PathBuf;
use std::process::Command;
mod error;
mod tests;

const ENCRYPTED: &str = "[ENCRYPTED]";

/// Easily add and execute aliases for long commands.
///
/// Example usage:
/// $ ea add my_alias echo {{variable}} {{another_variable}}
/// $ ea my_alias Hello world
/// > Hello world
/// $ ea my_alias another_variable=Hello variable=world
/// > world Hello
#[derive(Debug, Parser, Default)]
#[command(
    author,
    version,
    about,
    long_about,
    args_conflicts_with_subcommands = true,
    verbatim_doc_comment
)]
struct Cli {
    #[command(flatten)]
    cmd: Option<Cmd>,

    #[command(subcommand)]
    ctype: Option<CommandType>,

    #[clap(skip = {[dirs::home_dir().unwrap().to_str().unwrap(), ".config", "ea", "earc"].iter().collect::<PathBuf>()})]
    config_file: PathBuf,
}

#[derive(Args, Debug, Default)]
struct Cmd {
    /// The alias used to invoke a command.
    alias: String,
    /// A space separated list of variables to be substituted in the stored commad.
    /// The variables can either be positional or named arguments, but not both.
    /// If by name, they should be formatted
    /// x=value_x y=value_y
    ///
    /// If by value, you can simply enter
    /// value_x value_y
    ///
    /// Values that contain spaces should be quoted, e.g.
    /// x="value1 value2" y="value3 value4"
    ///
    /// See `ea help add` for more info on defining and using variables.
    #[clap(verbatim_doc_comment)]
    vars: Vec<String>,
}

#[derive(Subcommand, Debug, Clone, PartialEq)]
enum CommandType {
    /// Add a new alias with its corresponding command.
    Add {
        /// The alias used to invoke a command. It should not contain any spaces.
        alias: String,
        /// The command to be invoked by the alias. The command can contain spaces. It can also
        /// contain variables to be substituted. Variables should be input in the form `{{name}}`
        /// (without the backticks) so that when the command is invoked, substitutions can be
        /// invoked like `name=value`. You don't have to input variable names in the stored
        /// command, but you must either include names for all variables or none. If there are no
        /// variable names, substitutions will always be invoked positionally.
        ///
        /// You can include the same variable name more than once in a command, but if you do,
        /// substitutions must always be made by name.
        ///
        /// Equal signs in variable values must be escaped as `==`.
        ///
        /// # Example Usage
        /// $ ea add positional_or_by_name echo {{x}} {{y}}
        /// $ ea positional_or_by_name y=world x=hello
        /// > hello world
        /// $ ea positional_or_by_name world hello
        /// world hello
        ///
        /// $ ea add positional_only echo {{}} {{}}
        /// $ ea positional_only "it is" "what it is"
        /// > it is what it is
        /// $ ea positional_only x==y y==x
        /// > x=y y=x
        ///
        /// $ ea add by_name_only echo {{x}} {{y}} {{x}}
        /// $ ea echo x="it is" y=what
        /// > it is what it is
        #[clap(verbatim_doc_comment)]
        command: Vec<String>,

        /// Pass this flag in order to add the command to your system's keychain.
        #[clap(short, long, default_value_t = false)]
        secure: bool,
    },
    /// Permanently remove the provided alias.
    Remove { alias: String },
    /// List all available plaintext aliases along with the commands that they invoke.
    /// Encrypted commands will not be decrypted by default.
    List {
        /// Also display commands stored in the keyring in cleartext.
        #[clap(short, long, default_value_t = false)]
        secure: bool,
    },
    /// Show the definition for a particular alias.
    /// Will always decrypt encrypted commands for display.
    Show {
        /// Alias whose command will be shown.
        alias: String,
    },
    /// Modify an existing alias inplace.
    Modify {
        /// Alias to be modified.
        alias: String,
    },

    /// Encrypt an existing alias's command and add it to your keyring
    Encrypt {
        /// The alias to be encrypted
        alias: String,
    },

    /// Decrypt an encrypted command and add it back to your normal config
    Decrypt {
        /// The alias whose command will be decrypted
        alias: String,
    },
}

impl Cli {
    fn process(&mut self) -> Result<Option<String>, CliError> {
        if self.cmd.as_ref().is_some_and(|a| !a.alias.is_empty()) {
            self.execute()?;
            Ok(None)
        } else {
            let res = match &self.ctype {
                Some(CommandType::Add {
                    alias,
                    command,
                    secure,
                }) => self.add(alias, command, *secure),
                Some(CommandType::Remove { alias }) => self.remove(alias),
                Some(CommandType::List { secure }) => self.list(None, *secure),
                Some(CommandType::Show { alias }) => self.list(Some(alias), true),
                Some(CommandType::Modify { alias }) => self.modify(alias),
                Some(CommandType::Encrypt { alias }) => self.encrypt(alias),
                Some(CommandType::Decrypt { alias }) => self.decrypt(alias),
                _ => unreachable!(),
            };
            match res {
                Ok(s) => Ok(Some(s)),
                Err(e) => Err(e),
            }
        }
    }

    fn add<'a>(
        &'a self,
        alias: &'a str,
        command: &Vec<String>,
        secure: bool,
    ) -> Result<String, CliError> {
        let mut config = self.get_config()?;
        if [
            "add", "remove", "show", "list", "modify", "help", "encrypt", "decrypt",
        ]
        .contains(&alias)
        {
            return Err(CliError::ReservedKeyword);
        }
        if let Some(existing_line) = config
            .split('\n')
            .find(|s| s.starts_with(&format!("{}::", alias)))
        {
            println!("Alias already exists: {}", existing_line);
            let mut confirmation;
            loop {
                confirmation = rustyline::DefaultEditor::new()
                    .unwrap()
                    .readline("Would you like to overwrite the existing alias (Y/n)? ")?;
                if confirmation.to_lowercase() == "n" {
                    return Ok("Alias not modified.".to_string());
                } else if confirmation.to_lowercase() == "y" {
                    self.remove(alias)?;
                    println!("Old command removed.");
                    config = self.get_config()?;
                    break;
                } else {
                    println!("Invalid input.");
                }
            }
        }
        if command.is_empty() {
            return Err(CliError::CommandRequired);
        }
        // Validate subs in command: if one contains a var name, they should all contain var names
        config = config.trim().to_string();
        let re_with_name = Regex::new(r"(\{\{\w+}\})").unwrap();
        let re_without_name = Regex::new(r"(\{\{}\})").unwrap();
        let re_all = Regex::new(r"(\{\{\w*}\})").unwrap();
        let command = command
            .iter()
            .fold(String::new(), |command, word| command + word + " ")
            .trim()
            .to_string();
        let all_caps: Vec<String> = re_all
            .find_iter(&command)
            .map(|c| c.as_str().to_string())
            .collect();
        if re_with_name.is_match(&command) && re_without_name.is_match(&command) {
            return Err(CliError::MixingSubTypes(all_caps));
        }

        // Make sure all vars have valid names
        let re_invalid = Regex::new(r"\{\{.*?\}\}").unwrap();
        let invalid_caps = re_invalid
            .find_iter(&command)
            .map(|c| c.as_str().to_string())
            .collect::<Vec<_>>();
        if invalid_caps.len() > all_caps.len() {
            return Err(CliError::InvalidVariables(all_caps, invalid_caps));
        }

        let mut conf_vec = config
            .split('\n')
            .map(|l| l.to_string())
            .filter(|s| !s.is_empty())
            .collect::<Vec<String>>();

        // If the command is to be encrypted, we store the actual command in the keyring
        // and just leave ENCRYPTED in the config file.
        let command = if secure {
            let res = self.add_to_keyring(alias, &command);
            if res.is_err() {
                return Err(CliError::Keyring(format!(
                    "Failed to add alias/command to keyring: {alias}::{command}"
                )));
            }
            ENCRYPTED.to_string()
        } else {
            command
        };

        conf_vec.push(format!("{}::{}", alias, command));
        conf_vec.sort_by(|a, b| a[..a.find("::").unwrap()].cmp(&b[..b.find("::").unwrap()]));
        config = conf_vec
            .iter()
            .fold(String::new(), |conf, l| conf + l + "\n")
            .trim()
            .to_string();
        std::fs::write(&self.config_file, config.as_bytes())?;
        Ok(format!("Alias added successfully: {alias}."))
    }

    fn remove<'a>(&'a self, alias: &'a str) -> Result<String, CliError> {
        let config = self.get_config()?;
        let config = config.trim();
        let line_count = config.split('\n').count();
        'a: for line in config.split('\n') {
            if line == &format!("{}::{}", alias, ENCRYPTED) {
                if let Err(x) = self.remove_from_keyring(alias) {
                    let mut confirmation;
                    println!("{x}\nFailed to remove command from keyring.");
                    loop {
                        confirmation = rustyline::DefaultEditor::new().unwrap().readline(
                            "Would you still like to remove the entry from the config file (Y/n)? ",
                        )?;
                        if confirmation.to_lowercase() == "n" {
                            return Ok("Config file unchanged.".to_string());
                        } else if confirmation.to_lowercase() == "y" {
                            break 'a;
                        } else {
                            println!("Invalid input.");
                        }
                    }
                }
                break 'a;
            }
        }
        let new_config = config
            .split('\n')
            .filter(|line| !line.starts_with(&format!("{}::", alias)))
            .fold(String::new(), |conf, new_line| conf + new_line + "\n");
        let new_config = new_config.trim();
        let new_line_count = new_config.split('\n').count();
        if line_count != new_line_count {
            std::fs::write(&self.config_file, new_config)?;
            Ok("Alias removed successfully.".to_string())
        } else {
            Ok("Alias not found.".to_string())
        }
    }

    fn list<'a>(
        &'a self,
        alias: Option<&'a String>,
        display_secure: bool,
    ) -> Result<String, CliError> {
        let config = self.get_config()?;
        let stuff_to_print = match alias {
            None => {
                if display_secure {
                    self.decrypt_config(config)?
                } else {
                    config
                }
            }
            Some(a) => {
                let (cmd, _) = self.retrieve_command(a)?;
                cmd
            }
        };

        Ok(stuff_to_print)
    }

    fn modify<'a>(&'a self, alias: &'a str) -> Result<String, CliError> {
        let (cmd, secure) = self.retrieve_command(alias)?;
        let new_cmd = rustyline::DefaultEditor::new()
            .unwrap()
            .readline_with_initial("> ", (&cmd, ""))?;
        let new_cmd = new_cmd.trim();
        self.remove(alias)?;
        if let Err(e) = self.add(
            alias,
            &new_cmd.split(' ').map(|x| x.to_string()).collect(),
            secure,
        ) {
            self.add(
                alias,
                &cmd.split(' ').map(|x| x.to_string()).collect(),
                secure,
            )?;
            return Err(e);
        }
        Ok("Alias modified successfully.".to_string())
    }

    fn bootstrap(&self) -> Result<(), CliError> {
        let parent_dir = self.config_file.parent().unwrap();
        if parent_dir.is_file() {
            return Err(CliError::ConfigDirIsFile);
        }
        if !parent_dir.exists() {
            std::fs::create_dir(parent_dir)?;
            println!("Created parent directory for config: {:?}", &parent_dir);
        }
        if !self.config_file.exists() {
            std::fs::write(&self.config_file, [])?;
            println!("Created empty config file at {:?}", &self.config_file);
        }
        Ok(())
    }

    fn get_config(&self) -> Result<String, CliError> {
        if !self.config_file.exists() {
            self.bootstrap()?;
        }
        let bytes = std::fs::read(&self.config_file).unwrap();
        String::from_utf8(bytes).map_err(Into::into)
    }

    fn decrypt_config<'a>(&'a self, config: String) -> Result<String, CliError> {
        let mut decrypted = String::new();

        for line in config.lines() {
            let (alias, cmd_str) = line.split_once("::").unwrap();
            let cmd: String;
            if cmd_str == ENCRYPTED {
                let res = self.get_from_keyring(&alias);
                if res.is_err() {
                    return Err(CliError::Keyring(format!(
                        "Failed to retrieve alias: {alias}"
                    )));
                }
                cmd = res.unwrap();
            } else {
                cmd = cmd_str.to_string()
            }

            decrypted.push_str(&format!("{}::{}\n", alias, cmd));
        }
        Ok(decrypted.trim_end().to_string())
    }

    fn retrieve_command<'a>(&'a self, alias: &'a str) -> Result<(String, bool), CliError> {
        let config = self.get_config()?;
        let cmd_line = config
            .split('\n')
            .find(|x| x.starts_with(&(format!("{}::", alias))))
            .ok_or(CliError::AliasNotFound(alias))?;
        let mut cmd = cmd_line.split_once("::").unwrap().1.to_string();
        let is_secure: bool;
        (cmd, is_secure) = if cmd == ENCRYPTED {
            let decrypted = self.get_from_keyring(alias)?;
            (decrypted, true)
        } else {
            (cmd, false)
        };
        Ok((cmd.to_string(), is_secure))
    }

    #[cfg(all(target_os = "linux", not(test)))]
    fn get_keyring_entry(alias: &str) -> Result<Entry, CliError> {
        Entry::new_with_target("easy_alias", "easy_alias", alias).map_err(Into::into)
    }

    #[cfg(target_os = "macos")]
    fn get_keyring_entry(alias: &str) -> Result<Entry, CliError> {
        Entry::new("easy_alias", alias).map_err(Into::into)
    }

    #[cfg(test)]
    fn get_keyring_entry(alias: &str) -> Result<Entry, CliError> {
        keyring::set_default_credential_builder(mock::default_credential_builder());
        let entry = Entry::new_with_target("easy_alias", "easy_alias", alias)?;
        if alias == "echo_secure" {
            entry.set_password("echo hello secure")?;
        } else if alias == "echo" {
            entry.set_password("echo hello")?;
        } else if alias == "echo_secure_subs" {
            entry.set_password("echo {{x}} secure")?;
        }
        Ok(entry)
    }

    fn get_from_keyring<'a>(&'a self, alias: &'a str) -> Result<String, CliError> {
        let entry = Self::get_keyring_entry(alias)?;
        entry.get_password().map_err(Into::into)
    }

    fn remove_from_keyring<'a>(&'a self, alias: &'a str) -> Result<(), CliError> {
        let entry = Self::get_keyring_entry(alias)?;
        entry.delete_password().map_err(Into::into)
    }

    fn add_to_keyring<'a>(&'a self, alias: &'a str, cmd: &'a str) -> Result<(), CliError> {
        let entry = Self::get_keyring_entry(alias)?;
        entry.set_password(cmd).map_err(Into::into)
    }

    fn perform_subs(&self, args: String) -> Result<String, CliError> {
        let subs = self
            .cmd
            .as_ref()
            .unwrap()
            .vars
            .iter()
            .map(|s| {
                let mut new_s = s.to_string();
                while let Some(ind) = new_s.rfind("==") {
                    new_s.replace_range(ind..=ind + 1, "<EQUALS>")
                }
                new_s
            })
            .collect::<Vec<String>>();

        let re_all = Regex::new(r"\{\{\w*}\}").unwrap();
        if !re_all.is_match(&args) && subs.is_empty() {
            return Ok(args);
        }

        let mut new_args = args.clone();

        let all_positional = subs.iter().all(|sub| !sub.contains('='));
        let all_by_name = subs.iter().all(|sub| sub.contains('='));
        if !(all_positional || all_by_name) {
            return Err(CliError::MixingSubTypes(subs));
        }

        // Check for repeat variables in stored command because repeat variables require all named args.
        let re_named = Regex::new(r"\{\{\w+}\}").unwrap();
        let mut named_caps: Vec<String> = re_named
            .find_iter(&new_args)
            .map(|c| c.as_str().to_string())
            .collect();
        named_caps.sort();
        let init_len = named_caps.len();
        named_caps.dedup();
        if named_caps.len() != init_len && !all_by_name {
            return Err(CliError::AllNamedRequired(named_caps));
        }

        let caps = if named_caps.is_empty() {
            re_all
                .find_iter(&args)
                .map(|c| c.as_str().to_string())
                .collect()
        } else {
            named_caps
        };
        if caps.len() != subs.len() {
            return Err(CliError::SubsMismatch(
                caps,
                &self.cmd.as_ref().unwrap().vars,
            ));
        }

        if all_positional {
            subs.iter().zip(caps.iter()).for_each(|(val, name)| {
                new_args = new_args.replacen(name, &val.replace("<EQUALS>", "="), 1);
            });
        } else if all_by_name {
            for sub in subs.iter() {
                let (sub_name, sub_val) = sub.split_once('=').unwrap();
                let sub_name = format!("{{{{{}}}}}", sub_name);
                let sub_val = sub_val.replace("<EQUALS>", "=");
                if !caps.contains(&sub_name) {
                    return Err(CliError::SubsMismatch(
                        caps,
                        &self.cmd.as_ref().unwrap().vars,
                    ));
                }
                new_args = new_args.replace(&sub_name, &sub_val);
            }
        }
        Ok(new_args)
    }

    fn encrypt<'a>(&'a self, alias: &'a str) -> Result<String, CliError> {
        let (cmd, _) = self.retrieve_command(alias)?;
        self.remove(alias)?;
        self.add(
            alias,
            &cmd.split(' ').map(|x| x.to_string()).collect(),
            true,
        )?;
        Ok("Command obscured in config and added to keyring.".to_string())
    }

    fn decrypt<'a>(&'a self, alias: &'a str) -> Result<String, CliError> {
        let (cmd, _) = self.retrieve_command(alias)?;
        self.remove(alias)?;
        self.add(
            alias,
            &cmd.split(' ').map(|x| x.to_string()).collect(),
            false,
        )?;
        Ok("Command decrypted in config and removed from keyring.".to_string())
    }

    fn execute(&self) -> Result<(), CliError> {
        let (command, _) = self.retrieve_command(&self.cmd.as_ref().unwrap().alias)?;
        let args = self.perform_subs(command)?;
        let _ = Command::new("sh")
            .arg("-c")
            .arg(args)
            .status()
            .expect("Failed to execute command!");
        Ok(())
    }
}

fn main() {
    let mut cli = Cli::parse();
    let res = cli.process();
    match &res {
        Ok(Some(s)) => println!("{}", &s),
        Err(e) => println!("{e}"),
        _ => (),
    }
}

# Usage
## Basic Usage
To add an alias for a bash command, type
```bash
ea add <alias> <bash cmd>
```
To invoke the command, simply input
```bash
ea <alias>
```

## Substitutions
Easy Alias supports substitutions by writing variables in the form `{{}}` or `{{name}}` in the
command. To pass in the substitutions, simply add the substitutions after the initial alias separated by spaces.
```bash
ea add e echo {{x}} {{y}}
ea e Hello world!
> Hello world!
ea e y=Hello x=world!
> world! Hello
```

## Secure Commands
Don't want certain commands sitting around in the cleartext config? You can choose to add commands to your keyring.
You can do this either with the `--secure` or `-s` flag when adding the command, or by using the
`encrypt` subcommand.
```bash
ea add --secure secret "echo super secret"
ea list
>secret::[ENCRYPTED]
ea decrypt secret
ea list
>secret::echo super secret
ea encrypt secret
ea list
>secret::[ENCRYPTED]
```
You can also list all commands (including commands stored in the keyring) with the `--secure` or `-s` flags:
```bash
ea list -s
>secret::echo super secret
```

## More Commands
For more info, simply see `ea help` or `ea help <subcommand>`.

# Installation
## From the Terminal
Linux x86-64:
```bash
mkdir -p ~/easy_alias
curl https://gitlab.com/api/v4/projects/46308752/packages/generic/easy_alias_linux_x86-64/3.0.0/ea --output ~/easy_alias/ea
chmod +x ~/easy_alias/ea
echo "export PATH=\"~/easy_alias:\$PATH\"" >> ~/.bashrc
source ~/.bashrc
```

Mac x86-64:
```bash
mkdir -p ~/easy_alias
curl https://gitlab.com/api/v4/projects/46308752/packages/generic/easy_alias_mac_x86-64/3.0.0/ea --output ~/easy_alias/ea
chmod +x ~/easy_alias/ea
echo "export PATH=\"~/easy_alias:\$PATH\"" >> ~/.bashrc
source ~/.bashrc
```

Mac ARM64:
```bash
mkdir -p ~/easy_alias
curl https://gitlab.com/api/v4/projects/46308752/packages/generic/easy_alias_mac_ARM64/3.0.0/ea --output ~/easy_alias/ea
chmod +x ~/easy_alias/ea
echo "export PATH=\"~/easy_alias:\$PATH\"" >> ~/.bashrc
source ~/.bashrc
```

## From the Release Page
Download the appropriate binary from the [Release Page](https://gitlab.com/kylesower/easy_alias/-/releases),
navigate to the download directory, and run the following:
```bash
chmod +x ea
echo "export PATH=\"$(pwd):\$PATH\"" >> ~/.bashrc
source ~/.bashrc
```
Alternatively, you could of course put the binary wherever you want as long as it's in your PATH.

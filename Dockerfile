FROM rust:latest
RUN apt-get update
RUN apt-get install -y mingw-w64 \
                       clang \
                       cmake \
                       patch \
                       gcc \
                       g++ \
                       lzma-dev \
                       libxml2-dev \
                       libssl-dev \
                       xz-utils \
                       bzip2 \
                       cpio \
                       zlib1g-dev

RUN rustup target add x86_64-pc-windows-gnu
RUN rustup target add x86_64-apple-darwin
RUN rustup target add aarch64-apple-darwin
RUN git clone https://github.com/tpoechtrager/osxcross
WORKDIR /osxcross
RUN wget https://github.com/joseluisq/macosx-sdks/releases/download/13.1/MacOSX13.1.sdk.tar.xz
RUN mv MacOSX13.1.sdk.tar.xz tarballs/
ENV UNATTENDED yes
ENV PATH /osxcross/target/bin:$PATH
ENV OSX_VERSION_MIN 13.0
RUN ./build.sh
